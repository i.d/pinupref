package com.pinup.ref.app

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class CompanyFragment : Fragment(R.layout.fragment_company){
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val questions = resources.getStringArray(R.array.titles)
        val answers = resources.getStringArray(R.array.contents)
        view.findViewById<RecyclerView>(R.id.rvCompany).apply {
            adapter = DefaultAdapter(true).also { it.submitList(List(questions.size){index ->
                DefaultCard(questions[index],answers[index])
            }) }
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
    }
}