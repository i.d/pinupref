package com.pinup.ref.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentTransaction
import com.yandex.metrica.impl.ob.lo

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavSet()
        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentView,main)
            .add(R.id.fragmentView,info)
            .add(R.id.fragmentView,help)
            .add(R.id.fragmentView, loader)
            .hide(info)
            .hide(help)
            .hide(main)
            .commit()
        isSupportShown(false)
    }

    fun isSupportShown(isShown: Boolean){
        if(isShown){
            supportFragmentManager.beginTransaction().show(main).hide(loader).commit()
        }
        findViewById<ImageView>(R.id.ivLogo).isVisible = isShown
        findViewById<View>(R.id.bottomBack).isVisible = isShown
        ivHelp.isVisible = isShown
        ivMain.isVisible = isShown
        ivInfo.isVisible = isShown
    }

    private val info: CompanyFragment = CompanyFragment()
    private val main: MainFragment = MainFragment()
    private val help: QuestionsFragment = QuestionsFragment()
    private val loader: LoaderFragment = LoaderFragment()
    private lateinit var ivHelp: ImageView
    private lateinit var ivMain: ImageView
    private lateinit var ivInfo: ImageView

    private var transaction: FragmentTransaction? = null

    private fun bottomNavSet() {
        ivHelp = findViewById(R.id.ivHelp)
        ivMain = findViewById(R.id.ivMain)
        ivInfo = findViewById(R.id.ivInfo)

        ivHelp.setOnClickListener {
            deselect()
            transaction?.show(help)?.commit()
            ivHelp.setColorFilter(resources.getColor(R.color.light_red,null))
        }
        ivMain.setOnClickListener {
            deselect()
            transaction?.show(main)?.commit()
            ivMain.setImageResource(R.drawable.center_selected)
        }
        ivInfo.setOnClickListener {
            deselect()
            transaction?.show(info)?.commit()
            ivInfo.setColorFilter(resources.getColor(R.color.light_red,null))
        }
    }



    private fun deselect(){
        transaction = supportFragmentManager.beginTransaction()
            .hide(info)
            .hide(main)
            .hide(help)
        ivHelp.setColorFilter(resources.getColor(R.color.light_black,null))
        ivInfo.setColorFilter(resources.getColor(R.color.light_black,null))
        ivMain.setImageResource(R.drawable.center_unselected)
    }
}