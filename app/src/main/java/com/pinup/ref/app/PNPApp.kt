package com.pinup.ref.app

import android.app.Application
import com.onesignal.OneSignal
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import org.sumon.eagleeye.EagleEyeApp

class PNPApp : EagleEyeApp(){
    override fun onCreate() {
        super.onCreate()
        YandexMetrica.activate(
            applicationContext,
            YandexMetricaConfig.newConfigBuilder("e197fd5e-2b24-42c8-a885-3d44f49ce5b7").build()
        )
        YandexMetrica.enableActivityAutoTracking(this)

        OneSignal.setAppId("994deff9-a78d-4b2d-b94e-7e5e04b9c889")
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        OneSignal.initWithContext(applicationContext)

    }
}