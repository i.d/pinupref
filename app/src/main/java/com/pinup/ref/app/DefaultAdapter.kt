package com.pinup.ref.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.core.view.marginTop
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class DefaultAdapter(private val isStatic: Boolean) : ListAdapter<DefaultCard, DefaultAdapter.DefVH>(Diff) {
    inner class DefVH(val root: View) : RecyclerView.ViewHolder(root)
    object Diff : DiffUtil.ItemCallback<DefaultCard>() {
        override fun areItemsTheSame(oldItem: DefaultCard, newItem: DefaultCard) =
            oldItem == newItem
        override fun areContentsTheSame(oldItem: DefaultCard, newItem: DefaultCard) =
            oldItem == newItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DefVH {
        return DefVH(
            LayoutInflater.from(parent.context).inflate(R.layout.item_default, parent, false)
        )
    }
    override fun onBindViewHolder(holder: DefVH, position: Int) {
        val item = getItem(position)
        holder.root.apply {
            findViewById<View>(R.id.header).setBackgroundColor(
                if(!isStatic)resources.getColor(R.color.white,null)
                else resources.getColor(R.color.light_red,null)
            )
            val desc = findViewById<TextView>(R.id.tvDescription)
            findViewById<TextView>(R.id.tvTitle).text = item.title
            desc.text = item.description
            if(!isStatic){
                setOnClickListener {
                    item.isOpened = !item.isOpened
                    desc.isVisible = item.isOpened
                }
            }else{
                desc.isVisible = true
            }
        }
    }
}