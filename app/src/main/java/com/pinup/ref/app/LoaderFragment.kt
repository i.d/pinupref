package com.pinup.ref.app

import android.animation.ObjectAnimator
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import kotlinx.coroutines.delay

class LoaderFragment : Fragment(R.layout.fragment_loader){
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val progressBar: ProgressBar = view.findViewById(R.id.pbLoader)
        webView = view.findViewById(R.id.noWebView)
        getFirebase()
        ObjectAnimator.ofInt(progressBar,"progress",0,10,35,100).apply {
            duration = 7000
            start()
        }
        lifecycleScope.launchWhenCreated {
            delay(7000)
            (requireActivity() as MainActivity).apply {
                if(retLink != "nothing" && isChecked){
                    requireActivity().supportFragmentManager.commit {
                        add(R.id.fragmentView,WebFragment())
                    }
                }else {
                    isSupportShown(true)
                }
            }
        }

    }

    lateinit var webView: WebView

    companion object {
        var retLink: String = ""
    }
    private var isChecked = false
    private fun getFirebase(){
        val config = FirebaseRemoteConfig.getInstance()
        config.setConfigSettingsAsync(FirebaseRemoteConfigSettings.Builder().setFetchTimeoutInSeconds(2400).build())
        config.setDefaultsAsync(mapOf(
            "def_link" to "nothing",
            "def_domen" to "null"
        ))
        config.fetchAndActivate().addOnSuccessListener {
            retLink = config.getString("def_link")
            if(retLink != "nothing"){
                webView.webViewClient = object : WebViewClient(){
                    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                        super.onPageStarted(view, url, favicon)
                        val domen = config.getString("def_domen")
                        if (url?.contains(domen) == true || url?.contains("http") == false) {
                            retLink = "nothing"
                            return
                        }
                        isChecked = true
                    }
                }
                webView.loadUrl(retLink)
            }
        }
    }
}