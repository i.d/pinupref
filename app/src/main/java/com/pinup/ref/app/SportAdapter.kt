package com.pinup.ref.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class SportAdapter : ListAdapter<SportWrapper, SportAdapter.SportVH>(Diff) {
    inner class SportVH(val root: View) : RecyclerView.ViewHolder(root)
    object Diff : DiffUtil.ItemCallback<SportWrapper>() {
        override fun areItemsTheSame(oldItem: SportWrapper, newItem: SportWrapper) =
            oldItem.sportCard == newItem.sportCard
        override fun areContentsTheSame(oldItem: SportWrapper, newItem: SportWrapper) =
            oldItem.sportCard == newItem.sportCard
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SportVH {
        return SportVH(
            LayoutInflater.from(parent.context).inflate(R.layout.item_sport, parent, false)
        )
    }

    override fun onBindViewHolder(holder: SportVH, position: Int) {
        getItem(position).setCard(holder.root as CardView)
        holder.root.setOnClickListener {
            getItem(position).onClick()
        }
    }
}