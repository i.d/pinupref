package com.pinup.ref.app

data class Match(
    val firstCountry: String,
    val secondCountry: String,
    val time: String,
    val firstCoefficient: Float,
    val secondCoefficient: Float,
    val drawCoefficient: Float
)

data class SportCard(val title: String, val matches: List<Match>, var isOpened: Boolean = false)

data class DefaultCard(val title: String, val description: String, var isOpened: Boolean = false)