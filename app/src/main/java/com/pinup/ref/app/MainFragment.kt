package com.pinup.ref.app

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.random.Random

class MainFragment : Fragment(R.layout.fragment_main) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapt(view.findViewById(R.id.rvSports))
    }

    private fun adapt(recyclerView: RecyclerView) {
        countries = resources.getStringArray(R.array.countries).toMutableList()
        countries.shuffle(random)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = SportAdapter().also {
            it.submitList(listOf(
                createCard(R.string.football,3,true),
                createCard(R.string.basketball,2,false),
                createCard(R.string.hockey,2,true),
            ).map { SportWrapper(it) })
        }
        recyclerView.setHasFixedSize(true)

    }

    private lateinit var countries: MutableList<String>

    private val random
        get() = Random(System.nanoTime())

    private fun createMatch(time: String): Match{
        val coef = random.nextDouble(2.0,4.0).toFloat()
        val midCoef = random.nextDouble(6.0,10.0).toFloat()
        return Match(countries.removeLast(),countries.removeLast(),time,coef,6-coef,midCoef)
    }

    private fun createCard(@StringRes title: Int, count: Int, isOpened: Boolean): SportCard{
        val matches = mutableListOf<Match>()
        repeat(count){
            matches.add(when(title){
                R.string.football -> createMatch(random.nextInt(20,92).toString())
                R.string.basketball -> createMatch(random.nextInt(8,38).toString())
                R.string.hockey -> createMatch(random.nextInt(12,60).toString())
                else -> throw IllegalStateException("Unknown state")
            })
        }
        return SportCard("${getString(title)} ($count)",matches,isOpened)
    }
}