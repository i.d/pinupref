package com.pinup.ref.app

import android.view.Gravity
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.view.get
import androidx.core.view.isVisible

class SportWrapper(private val card: SportCard) {
    private lateinit var title: TextView
    private lateinit var arrow: ImageView
    private lateinit var tableLayout: TableLayout
    lateinit var sportCard: SportCard
    private lateinit var cardView: CardView

    fun setCard(cardView: CardView) {
        this.cardView = cardView
        title = cardView.findViewById(R.id.tvTitle)
        arrow = cardView.findViewById(R.id.ivArrow)
        tableLayout = cardView.findViewById(R.id.tableLayout)
        sportCard = card
        title.text = card.title
        if(sportCard.isOpened){
            onClick()
            onClick()
        }
        card.matches.forEach {
            tableLayout.addView(createRow(it), TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT).also {
                it.topMargin = 8
                it.bottomMargin = 8
            })
        }
    }

    private fun createRow(match: Match): TableRow {
        val row = TableRow(tableLayout.context)
        row.addView(
            getTextView("${match.firstCountry}\n${match.secondCountry}"),
            getLayoutParams(4f)
        )
        row.addView(getTextView(match.time), getLayoutParams(2f))
        row.addView(getTextView("%.2f".format(match.firstCoefficient)), getLayoutParams(1f))
        row.addView(getTextView("%.2f".format(match.drawCoefficient)), getLayoutParams(1f))
        row.addView(getTextView("%.2f".format(match.secondCoefficient)), getLayoutParams(1f))
        return row
    }

    private fun getTextView(text: String) = TextView(tableLayout.context).apply {
        this.text = text
        setTextColor(resources.getColor(R.color.black, null))
        gravity = Gravity.CENTER
    }

    private fun getLayoutParams(weight: Float) = TableRow.LayoutParams(
        TableRow.LayoutParams.WRAP_CONTENT,
        TableRow.LayoutParams.MATCH_PARENT,
        weight
    )

    fun onClick() {
        sportCard.isOpened = !sportCard.isOpened
        tableLayout.isVisible = sportCard.isOpened
        arrow.setImageResource(
            if (sportCard.isOpened) R.drawable.arrow_up
            else R.drawable.arrow_down
        )
    }

}