package com.pinup.ref.app

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.view.View
import android.webkit.*
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultRegistry
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.yandex.metrica.impl.ob.S
import com.yandex.metrica.impl.ob.it
import org.sumon.eagleeye.EagleEye
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class WebFragment : Fragment(R.layout.fragment_web) {
    private lateinit var sLoader: WebView
    private lateinit var pbSiteLoader: ProgressBar
    private lateinit var snackbar: TextView

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sLoader = view.findViewById(R.id.sLoader)
        pbSiteLoader = view.findViewById(R.id.pbSiteProgress)
        snackbar = view.findViewById(R.id.sbFront)

        preWeb()

        EagleEye.getStatus(requireContext()) {
            snackbar.isVisible = !it
            if (it) {
                sLoader.reload()
                sLoader.setOnTouchListener(onTouchListenerDis)
            } else sLoader.setOnTouchListener(onTouchListenerEn)
        }

    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun preWeb() {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (sLoader.canGoBack()) {
                        sLoader.goBack()
                    } else {
                        isEnabled = false
                        requireActivity().onBackPressed()
                    }
                }
            })
        sLoader.loadUrl(LoaderFragment.retLink)
        sLoader.apply {

            CookieManager.setAcceptFileSchemeCookies(true)
            CookieManager.getInstance()
                .setAcceptThirdPartyCookies(this, true)

            settings.apply {
                builtInZoomControls = true
                displayZoomControls = false
                useWideViewPort = true
                loadWithOverviewMode = true

                javaScriptEnabled = true

                cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
                defaultTextEncodingName = "utf-8"

                domStorageEnabled = true
                allowContentAccess = true
                allowFileAccess = true
            }

            webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    if (url == null) return true
                    if (url.contains("mailto:") || url.contains("tel:")) {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                        return true
                    }
                    for (s in getSTS()) {
                        if (url.contains(s, true)) return true
                    }
                    loadUrl(url)
                    return true
                }


                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    pbSiteLoader.isVisible = true
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    pbSiteLoader.isVisible = false
                }
            }

            webChromeClient = object : WebChromeClient() {
                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    super.onProgressChanged(view, newProgress)
                    pbSiteLoader.progress = newProgress
                }


                override fun onShowFileChooser(
                    webView: WebView?,
                    filePathCallback: ValueCallback<Array<Uri>>?,
                    fileChooserParams: FileChooserParams?
                ): Boolean {
                    askPermission(Manifest.permission.CAMERA) {
                        askPhoto(it, filePathCallback)
                    }
                    return true
                }
            }
        }
    }

    private fun askPermission(permission: String, callback: (Boolean) -> Unit) {
        requireActivity().activityResultRegistry.register(
            System.nanoTime().toString(),
            ActivityResultContracts.RequestPermission(),
            callback
        )
            .launch(permission)
    }

    private fun askIntent(intent: Intent, callback: (ActivityResult) -> Unit) {
        requireActivity().activityResultRegistry.register(
            System.nanoTime().toString(),
            ActivityResultContracts.StartActivityForResult(),
            callback
        )
            .launch(intent)
    }

    private fun askPhoto(isCameraAllowed: Boolean, filePathCallback: ValueCallback<Array<Uri>>?) {
        if (!isCameraAllowed) askIntent(gallery) { callback(it, filePathCallback) }
        else {
            val uri = uniquePhotoFile()
            askIntent(
                Intent(Intent.ACTION_CHOOSER).apply {
                    putExtra(Intent.EXTRA_INTENT, camera(uri))
                    putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(gallery))
                }
            ) { callback(it, filePathCallback, uri) }
        }
    }

    private fun callback(
        result: ActivityResult,
        filePathCallback: ValueCallback<Array<Uri>>?,
        image: Uri? = null
    ) {
        if (result.resultCode == Activity.RESULT_OK && result.data != null && result.data!!.data != null) {
            filePathCallback.submit(result.data?.data)
        } else {
            if (result.resultCode == Activity.RESULT_OK) filePathCallback.submit(image)
            else filePathCallback.submit()
        }
    }

    private fun ValueCallback<Array<Uri>>?.submit(uri: Uri? = null) {
        if (uri == null) this?.onReceiveValue(arrayOf())
        else this?.onReceiveValue(arrayOf(uri))
    }

    private val gallery = Intent().apply {
        action = Intent.ACTION_GET_CONTENT
        type = "image/*"
        addCategory(Intent.CATEGORY_OPENABLE)
    }

    private fun camera(image: Uri) = Intent().apply {
        action = MediaStore.ACTION_IMAGE_CAPTURE
        putExtra(MediaStore.EXTRA_OUTPUT, image)
    }

    private fun uniquePhotoFile(
        pref: String = "PNUP_",
        name: String = System.currentTimeMillis().toString()
    ): Uri {
        val file = File(
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)?.absolutePath +
                    "/$pref$name.jpg"
        )
        return FileProvider.getUriForFile(
            requireContext(),
            "${context?.packageName}.provider",
            file
        )
    }

    @SuppressLint("ClickableViewAccessibility")
    private val onTouchListenerEn = View.OnTouchListener { _, _ -> true }

    @SuppressLint("ClickableViewAccessibility")
    private val onTouchListenerDis = View.OnTouchListener { _, _ -> false }

    companion object {
        const val STS =
            "WW1sMExteDUsIGJTNXRaUT09LCBkQzV0WlE9PSwgYVc1emRHRm5jbUZ0LCBkSGRwZEhSbGNnPT0sZVc5MWRIVmlaUT09LCAgZG1saVpYST0sWm1GalpXSnZiMnN1WTI5dCxkbXN1WTI5dA=="

        fun getSTS(): List<String> {
            val hashList = String(Base64.decode(STS, Base64.DEFAULT))
                .split(',')
            return List(hashList.size) {
                String(Base64.decode(hashList[it], Base64.DEFAULT))
            }
        }
    }
}