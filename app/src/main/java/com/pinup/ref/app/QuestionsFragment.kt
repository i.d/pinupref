package com.pinup.ref.app

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class QuestionsFragment : Fragment(R.layout.fragment_questions) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val questions = resources.getStringArray(R.array.questions)
        val answers = resources.getStringArray(R.array.answers)
        view.findViewById<RecyclerView>(R.id.rvQuestions).apply {
            adapter = DefaultAdapter(false).also {
                it.submitList(List(questions.size) { index ->
                    DefaultCard(questions[index], answers[index])
                })
            }
            layoutManager = LinearLayoutManager(context)
        }
    }
}